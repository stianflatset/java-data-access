# Java Assignment 2 - Java data access and display

### Created by Stian Tonning Flatset & Sigve Eilertsen

### Read customer data:
* Get all customers: localhost:8080/api/customers
* Get customer based on ID: localhost:8080/api/customers/id/{id}
* Get customer based on name: localhost:8080/api/customers/name{firstName}
* Get a certain number of customers: localhost:8080/api/customers/{limit}/{offset}
* Add a new customer to the database: localhost:8080/api/customers/add
* Update an existing customer: localhost:8080/api/customers/{id}
* Show number of customers in each country: localhost:8080/api/customers/countrylist
* Show the total the customers has spent: localhost:8080/api/customers/moneymakers
* Show a certain customers most listened to genre: localhost:8080/api/customers/genre/{id}

### Homepage:
* Displays 5 random artists, 5 random songs and 5 random genres.
* Search for a songname in the inputfield

### Search for song:
* When you search for a song the site redirects to localhost:8080/song/?search=(Your search)
* IF you remove the "?" in the URL it will display songinfo.
* Press the "Go back" button to return to homepage.