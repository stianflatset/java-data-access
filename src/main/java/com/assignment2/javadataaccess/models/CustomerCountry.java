package com.assignment2.javadataaccess.models;

public class CustomerCountry {
    private String country;
    private String number;

    public CustomerCountry(String country, String number) {
        this.country = country;
        this.number = number;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


}
