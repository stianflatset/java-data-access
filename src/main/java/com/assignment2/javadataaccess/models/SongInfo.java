package com.assignment2.javadataaccess.models;

public class SongInfo {
    private String title;
    private String artist;
    private String genre;
    private String album;
    private int length;

    public SongInfo (String title, String artist, String genre, String album, int length) {
        this.title = title;
        this.artist = artist;
        this.genre = genre;
        this.album = album;
        this.length = length;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
