package com.assignment2.javadataaccess.models;

public class CustomerGenre {
    private String genre;
    private String number;

    public CustomerGenre(String genre, String number) {
        this.genre = genre;
        this.number = number;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
