package com.assignment2.javadataaccess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaDataAccessApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaDataAccessApplication.class, args);
	}

}
