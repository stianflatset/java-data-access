package com.assignment2.javadataaccess.controllers;

import com.assignment2.javadataaccess.dap.CustomerRepository;
import com.assignment2.javadataaccess.models.Customer;
import com.assignment2.javadataaccess.models.CustomerCountry;
import com.assignment2.javadataaccess.models.CustomerSpender;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;

@RestController
public class CustomerController {

    // Configure some endpoints to manage crud
    private final CustomerRepository customerRepository;

    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    //Get all customers
    @RequestMapping(value="/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers(){
        return customerRepository.getAllCustomers();
    }

    //Get customers based on ID
    @RequestMapping(value="/api/customers/id/{id}", method = RequestMethod.GET)
    public Customer getCustomerById(@PathVariable int id) {
        return customerRepository.getCustomerById(id);
    }

    //Get customers based on name (firstname and lastname)
    @RequestMapping(value="/api/customers/name/{firstName}", method = RequestMethod.GET)
    public ArrayList<Customer> getCustomerByName(@PathVariable String firstName) {
        return customerRepository.getCustomerByName(firstName);
    }

    //Get certain number of customers - limit & offset
    @RequestMapping(value = "/api/customers/{limit}/{offset}", method = RequestMethod.GET)
    public ArrayList<Customer> getLimitedCustomers(@PathVariable int limit, @PathVariable int offset) {
        return customerRepository.getLimitedCustomers(limit, offset);
    }

    //Add a new customer to the database
    @RequestMapping(value = "api/customers/add", method = RequestMethod.POST)
    public Boolean addNewCustomer(@RequestBody Customer customer) {
        return customerRepository.addNewCustomer(customer);
    }

    //Updating an existing customer
    @RequestMapping(value = "api/customers/{id}", method = RequestMethod.PUT)
    public boolean updateCustomer(@PathVariable int id, @RequestBody Customer customer){
        return customerRepository.updateCustomer(customer);
    }

    //Showing number of customers in each country - ordered by desc
    @RequestMapping(value = "/api/customers/countrylist", method = RequestMethod.GET)
    public ArrayList<CustomerCountry> getCustomersInCountry() { return customerRepository.getCustomersInCountry(); }

    //Showing the total the customers has spent in descending.
    @RequestMapping(value = "/api/customers/moneymakers", method = RequestMethod.GET)
    public ArrayList<CustomerSpender> getCustomersTotalMoney() { return customerRepository.getCustomersTotalMoney();}

    //Showing a certain customers most listened to genre
    @RequestMapping(value = "/api/customers/genre/{id}", method = RequestMethod.GET)
    public String getCustomersPopularGenre(@PathVariable int id) {
        return customerRepository.getCustomersPopularGenre(id);
    }

}