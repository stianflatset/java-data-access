package com.assignment2.javadataaccess.controllers;

import com.assignment2.javadataaccess.dap.HomeRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

    HomeRepository hrep = new HomeRepository();

    //Getting 5 random artists, songs and genres to the home page
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) {
        model.addAttribute("artists", hrep.getFiveArtists());
        model.addAttribute("songs", hrep.getFiveSongs());
        model.addAttribute("genres", hrep.getFiveGenres());
        model.addAttribute("search", model);
        return "index";
    }
}
