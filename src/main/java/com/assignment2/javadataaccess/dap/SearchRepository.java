package com.assignment2.javadataaccess.dap;

import com.assignment2.javadataaccess.models.SongInfo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class SearchRepository {
    public int counter = 1;

    // Setting up the connection object we need.
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;


    public SongInfo getSongInfo(String title) {
        System.out.println("input: " + title);
        SongInfo songInfo = null;
        try {
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT tracks.Name AS 'songTitle', artists.Name AS 'artist', genres.Name AS 'genre', albums.Title AS 'albumTitle', Milliseconds \n" +
                            "FROM tracks, artists, albums, genres\n" +
                            "WHERE tracks.Name LIKE ? \n" +
                            "AND tracks.AlbumId = albums.AlbumId\n" +
                            "AND albums.ArtistId = artists.ArtistId\n" +
                            "AND tracks.GenreId = genres.GenreId LIMIT 1");
            preparedStatement.setString(1, "%" + title + "%");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                System.out.println(resultSet.getString("songTitle"));
                songInfo = new SongInfo(
                        resultSet.getString("songTitle"),
                        resultSet.getString("artist"),
                        resultSet.getString("genre"),
                        resultSet.getString("albumTitle"),
                        resultSet.getInt("Milliseconds")
                );
            }
            System.out.println("Going through the function for the: " + counter);
            System.out.println("Getting search data went well");
        } catch (Exception exception) {
            System.out.println("this error");
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println("this other error");
                System.out.println(exception.toString());
            }
        }
        System.out.println(songInfo);
        counter++;
        return songInfo;
    }
}
