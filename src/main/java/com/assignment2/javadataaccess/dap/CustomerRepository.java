package com.assignment2.javadataaccess.dap;

import com.assignment2.javadataaccess.models.Customer;
import com.assignment2.javadataaccess.models.CustomerCountry;
import com.assignment2.javadataaccess.models.CustomerSpender;

import java.util.ArrayList;
import java.util.HashMap;

public interface CustomerRepository {

    public ArrayList<Customer> getAllCustomers();
    public Customer getCustomerById(int id);
    public ArrayList<Customer> getCustomerByName(String firstName);
    public ArrayList<Customer> getLimitedCustomers(int limit, int offset);
    public boolean addNewCustomer(Customer customer);
    public boolean updateCustomer(Customer customer);
    public ArrayList<CustomerCountry> getCustomersInCountry();
    public ArrayList<CustomerSpender> getCustomersTotalMoney();
    public String getCustomersPopularGenre(int id);
}
