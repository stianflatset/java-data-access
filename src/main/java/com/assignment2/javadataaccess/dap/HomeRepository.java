package com.assignment2.javadataaccess.dap;

import com.assignment2.javadataaccess.logging.LogToConsole;
import com.assignment2.javadataaccess.models.Customer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class HomeRepository {

    // Setting up the connection object we need.
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;


    public ArrayList<String> getFiveArtists() {
        ArrayList<String> artistList = new ArrayList<>();
        try {
            // Connect to DB
            conn = DriverManager.getConnection(URL);

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM artists ORDER BY random() LIMIT 5");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                artistList.add(
                        resultSet.getString("Name")
                        );
            }
            System.out.println("Getting data went well");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return artistList;

    }

    public ArrayList<String> getFiveSongs() {
        ArrayList<String> songList = new ArrayList<>();
        try {
            // Connect to DB
            conn = DriverManager.getConnection(URL);

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM tracks ORDER BY random() LIMIT 5");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                songList.add(
                        resultSet.getString("Name")
                );
            }
            System.out.println("Getting data went well");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return songList;
    }

    public ArrayList<String> getFiveGenres() {
        ArrayList<String> genreList = new ArrayList<>();
        try {
            // Connect to DB
            conn = DriverManager.getConnection(URL);

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM genres ORDER BY random() LIMIT 5");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                genreList.add(
                        resultSet.getString("Name")
                );
            }
            System.out.println("Getting data went well");
        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return genreList;

    }
}
